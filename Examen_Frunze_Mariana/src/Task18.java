import java.util.Arrays;

public class Task18 {
    public static void main(String[] args) {
        //array de tip byte(care ocupa cel mai mic spatiu de memorie)
        byte[] arr = new byte[]{1, 2, 3, 4, 5};

        //afisarea array-ului initial
        System.out.println(Arrays.toString(arr));

        //elementul cu indicele 4, va fi schimbata valoarea cu 3
        arr[4] = 3;

        //afisarea array-ului modificat
        System.out.println(Arrays.toString(arr));
    }
}

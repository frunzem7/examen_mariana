package task8;

public class Main {
    public static void main(String[] args) {
        //creare de obiect cristinel si cristinuta
        Pufuleti cristinel = new Pufuleti();
        Pufuleti cristinuta = new Pufuleti();

        System.out.println("Pufuletii Critinel!");
        //apelarea metodelor cu ajutorul obiectului
        cristinel.addPufuletiInCamion();
        cristinel.callCamionMetro();
        cristinel.callCamionNr1();
        cristinel.callCamionLinella();
        System.out.println();

        System.out.println("Pufuletii Cristinuta!");
        //apelarea metodelor cu ajutorul obiectului
        cristinuta.addPufuletiInCamion();
        cristinuta.callCamionMetro();
        cristinuta.callCamionNr1();
        cristinuta.callCamionLinella();
    }
}

package task8;

public class Pufuleti {

    //metoda non-statica
    public void addPufuletiInCamion() {

        System.out.println("Am adaugat pufuletii in Camion!");
    }

    //metoda non-statica
    public void callCamionMetro() {

        System.out.println("Am chemat camionul la Metro!");
    }

    //metoda non-statica
    public void callCamionNr1() {

        System.out.println("Am chemat camionul la Nr1!");
    }

    //metoda non-statica
    public void callCamionLinella() {

        System.out.println("Am chemat camionul la Linella!");
    }
}

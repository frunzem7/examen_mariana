package task12;

public class Main {
    public static void main(String[] args) {

        //crearea obiectului cu parametru
        Person obj1 = new Person("Alina", 15);
        //crearea obiectului cu parametru
        Mariana obj2 = new Mariana("Mariana", 21, 2000);

        //getter pentru obj1
        System.out.println("Prima persoana se numeste: " + obj1.getName() + ".");
        System.out.println("Si are " + obj1.getAge() + " ani.");
        System.out.println();

        //getter pentru obj2
        System.out.println("A doua persoana se numeste: " + obj2.getName() + ".");
        System.out.println("Si are " + obj2.getAge() + " ani.");
        System.out.println("Este nascuta in anul: " + obj2.getYearOfBirthday() + ".");
    }
}

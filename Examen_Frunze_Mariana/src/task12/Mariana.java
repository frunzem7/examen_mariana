package task12;
//clasa Mariana mosteneste clasa Person
public class Mariana extends Person {
    //initializarea variabilei de tip int cu modificatorul de acces private
    private int yearOfBirthday;

    //constructor
    public Mariana(String name, int age, int yearOfBirthday) {
        super(name, age);
        this.yearOfBirthday = yearOfBirthday;
    }

    //metoda getter
    public int getYearOfBirthday() {
        return yearOfBirthday;
    }

    //metoda setter
    public void setYearOfBirthday(int newValue) {
        yearOfBirthday = newValue;
    }
}

package task12;

public class Person {

    //initializarea variabilor de tip int si String
    // cu modificatorul de acces private
    private String name;
    private int age;

    //constructor
    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    //metoda getter
    public String getName() {
        return name;
    }

    //metoda setter
    public void setName(String newValue) {
        name = newValue;
    }

    //metoda getter
    public int getAge() {
        return age;
    }

    //metoda setter
    public void setAge(int newValue) {
        age = newValue;
    }
}

public class Task1 {
    public static void main(String[] args) {
        //conversia din byte in short
        byte numberB = 12;
        short numberS = numberB;
        System.out.println(numberS);

        //conversia din int in double
        int numberI1 = 10000;
        double numberD = numberI1;
        System.out.println(numberD);

        //conversia din long in int
        long numberL = 200000;
        int numberI2 = (int) numberL;
        System.out.println(numberI2);

    }
}

public class Task5 {
    public static void main(String[] args) {
        //array de tip int
        int numbers[] = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100};
        int result = 0;

        //bucla for
        for (int i = 0; i < 10; i++)
            result = result + numbers[i];

        System.out.println("Suma elementelor: " + result);
    }
}

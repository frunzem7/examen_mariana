import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introdu un numar: ");
        long number = sc.nextLong();

        //constructia if-else
        if (number >= 0) {
            System.out.println("Numarul introdus de user este pozitiv!");
        } else {
            System.out.println("Numarul introdus de user este negativ!");
        }
    }
}

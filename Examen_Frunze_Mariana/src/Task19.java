public class Task19 {
    public static void main(String[] args) {
        //array de tip int
        int[] arr = new int[]{10, 20, 30, 40, 50};

        System.out.println("Primul element: ");
        System.out.println(arr[0]);

        System.out.println("Ultimul element: ");
        System.out.println(arr.length - 1);

        System.out.println("Lungimea array: ");
        System.out.println(arr.length);
    }
}

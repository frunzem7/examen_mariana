public class Task6 {
    public static void main(String[] args) {
        //O variabila de tip referential - String
        String text = "Republica Moldova este un stat situat în sud-estul Europei. mariana";

        System.out.println("Prima aparitie a literei m: " + text.indexOf("m"));
        System.out.println("Ultima aparitie a literei m: " + text.lastIndexOf("m"));
    }
}

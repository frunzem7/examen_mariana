import java.util.Scanner;

public class Task14 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        boolean trueFalse = true;
        //bulca do-while
        do {
            //exceptii java: try-catch
            try {
                System.out.print("Introdu un numar intreg: ");
                int number = sc.nextInt();

                System.out.println("Numarul introdus este " + number + ".");
                trueFalse = false;
            } catch (java.util.InputMismatchException exception) {
                System.out.println("Ai introdus un numar incorect, trebuie un numar intreg!");
                sc.next();
            }
        } while (trueFalse);
    }
}


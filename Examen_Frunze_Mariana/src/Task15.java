import java.util.Scanner;

public class Task15 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Introduceti ora 0-00: ");
        int ora = sc.nextInt();

        //constructia if-else
        if(ora>=0 && ora<6){
            System.out.println("Somnul");
        }else if(ora>=6 && ora<13){
            System.out.println("Timpul de trezit");
        }else if(ora>=13 && ora<17){
            System.out.println("Timpul de mancat");
        }else if (ora>=17 || ora<0){
            System.out.println("Timpul de culcat");
        }
    }
}

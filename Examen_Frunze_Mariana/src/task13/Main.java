package task13;

public class Main {
    public static void main(String[] args) {
        //crearea obiectului
        Pasare obj1 = new Pasare();
        //setter
        obj1.setName("Pasare");
        //getter
        System.out.println("Numele pasarei este " + obj1.getName() + ".");
        //apelarea metodei sunet(), cu ajutorul la obiect
        obj1.sunet();
        System.out.println();

        //crearea obiectului
        Lebada obj2 = new Lebada();
        //setter
        obj2.setName("Lebada");
        //getter
        System.out.println("Numele pasarei este " + obj2.getName() + ".");
        //apelarea metodei sunet(), cu ajutorul la obiect
        obj2.sunet();
    }
}

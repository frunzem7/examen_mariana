package task13;

public class Pasare {
    //initializarea variabilei de tip String,
    //cu modificatorul de acces private
    private String name;

    //metoda getter
    public String getName() {
        return name;
    }

    //metoda setter
    public void setName(String newValue) {
        name = newValue;
    }

    //metoda non-statica, sunet()
    public void sunet() {
        System.out.println("Pasarea canta!");
    }
}

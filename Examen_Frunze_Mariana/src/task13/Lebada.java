package task13;
//Clasa Lebada mosteneste de la clasa Pasare
public class Lebada extends Pasare {
    //principiul OOP, polimorfism
    @Override
    //metoda non-statica, sunet()
    public void sunet() {
        System.out.println("Lebada canta incet!");
    }
}

import java.util.Locale;
import java.util.Scanner;

public class Task11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String raspuns;

        //bulca do-while
        do {
            int a = 4;
            int b = 6;

            //constructia if-else
            if (a == b) {
                int result = a + b;
                System.out.println("Suma este: " + result);
            }else {
                System.out.println("Numerele nu sunt egale!");
                System.out.println();
            }
            System.out.println("Doriti sa se mai repete programul?");
            System.out.println("Da sau nu?: ");
            raspuns = sc.next();
        } while (raspuns.toLowerCase(Locale.ROOT).equals("da"));
        System.out.println("Nu ati introdus da sau nu!");
        System.out.println("Iesire din program");
    }
}

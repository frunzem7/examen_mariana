import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Introdu prima cifra: ");
        int number1 = sc.nextInt();

        System.out.print("Introdu a doua cifra: ");
        int number2 = sc.nextInt();

        System.out.print("Introdu a treia cifra: ");
        int number3 = sc.nextInt();

        //constructia if-else
        if (number1 == number2 && number2 == number3) {
            System.out.println("Toate numerele sunt egale!");
        } else if (number1 != number2 && number2 != number3) {
            System.out.println("Toate numerele sunt diferite!");
        } else {
            System.out.println("Nici nu sunt egale, nici diferite!");
        }
    }
}

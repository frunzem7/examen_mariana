public class Task9 {
    public static void main(String[] args) {
        //crearea de obiect
        Task9 obj = new Task9();

        //apelarea metodei cu ajutorul obiectului
        obj.perimetru();
    }

    //metoda non-statica, perimetru()
    public void perimetru() {
        int latura = 8;
        System.out.print("Perimetrul unui patrulater cu latura de " + latura + " cm este: ");
        System.out.print(latura + latura + latura + latura);
        System.out.println();
    }
}

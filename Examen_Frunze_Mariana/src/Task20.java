import java.util.Locale;
import java.util.Scanner;

public class Task20 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Scrie R,G sau V: ");
        String culori = sc.next().toUpperCase(Locale.ROOT);

        //declaratia switch
        switch (culori) {
            case "R":
                System.out.println("Rosu");
                break;
            case "G":
                System.out.println("Galben");
                break;
            case "V":
                System.out.println("Verde");
                break;
        }
    }
}
